﻿fuelModule.controller("FuelController", function ($scope, categoryRepository, choiceRepository, fuelItems) {
    $scope.categories = categoryRepository.query();
    $scope.choices = [];
    $scope.choiceExists = false;
    $scope.categoryName = "";

    $scope.selectCategory = function (category, $scope) {
        $scope.choices = choiceRepository.get(category.fuelCategoryID);
        $scope.categoryName = category.name;

        //- this is an "angular custom put" to match with web api put (for updating records)
        var $category = categoryRepository.get({ id: category.fuelCategoryID }, function(data){
            $id = $category.fuelCategoryID;
            $category.name = $category.name + "s";
            categoryRepository.update({ id: $id }, $category);

            category.name = $category.name;
            //$scope.apply();
        });

        

        //$scope.category = category;
        
        console.log(category.name);
    };
});

fuelModule.controller("CategoryController", function ($scope) {

});

//fuelModule.controller("ChoiceController", function ($scope, choiceRepository, ) {
//    $scope.choices = choices.get(id);
//});