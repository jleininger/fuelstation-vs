﻿
fs.controller('CategoryController', function ($scope, Category) {

    //$scope.category = Category.get({ id: category.fuelCategoryID }, function (data) { });
});

fs.controller('FuelCheckoutController', function ($scope, $filter, Checkout, Category, Athlete, SportCode, DefaultChoice, checkoutChoiceRepository, Choice) {
    //todo: is this necessary to run the the query thru network here?
    Checkout.refresh();
    Category.refresh();

    $scope.categories = Category.categories;
    $scope.checkouts = Checkout.checkouts;

    $scope.order = [];
    $scope.choices = [];

    $scope.studentId = '';
    $scope.studentSportId = null;
    $scope.sportCodeId = '';
    $scope.athleteData = '';
    $scope.fuelStage = null;
    var season = null;
    var choicesCopy = null;

    $scope.workoutStageComplete = _workoutStageComplete;
    $scope.workoutStageCancel = _workoutStageCancel;
    $scope.cancelCheckout = _cancelCheckout;


    $scope.deleteButton = function () {
        $scope.studentId = $scope.studentId.slice(0, $scope.studentId.length - 1)
    }

    $scope.numButton = function (num) {
        if ($scope.studentId.length < 9) {
            $scope.studentId = $scope.studentId + num;

            $scope.checkStudentId();
        }
        else {
            alert('Please clear the student ID by pressing the CLR button before entering a new one');
        };
    }

    $scope.checkStudentId = function () {
        if ($scope.studentId.length == 9) {
            $scope.athleteData = Athlete.getAthlete($scope.studentId);
            $scope.athleteData.$promise.then(

                function (result) {
                    $scope.athleteData = result;

                    if ($scope.athleteData.length > 0) {
                        //getPastCheckouts();
                        $scope.studentSportId = $scope.athleteData[0].studentSportID;
                        $scope.sportCodeId = $scope.athleteData[0].sportCodeID;
                        _workoutStageComplete();
                    }

                    else if ($scope.athleteData.length == 0) {
                        alert('Invalid student ID. Please correct the ID and try again');
                    }

                    //if ($scope.athleteData.length > 1)
                    //{
                    //    getPastCheckouts();
                    //    $('#workoutStageModal').modal('show');
                    //}
                    //else if ($scope.athleteData.length == 1)
                    //{
                    //    $scope.studentSportId = $scope.athleteData[0].studentSportID;
                    //    $scope.sportCodeId = $scope.athleteData[0].sportCodeID;

                    //    //getPastCheckouts();

                    //    if ($scope.athleteData[0].sportCode.sportDescription.indexOf("Generic") == -1)
                    //    {
                    //        getPastCheckouts();
                    //    }

                    //    $('#workoutStageModal').modal('show');

                    //}
                    //else if ($scope.athleteData.length == 0)
                    //{
                    //    alert('Invalid student ID. Please correct the ID and try again');
                    //}
                    

                },
                function (error) {
                    alert('There was an error. Please try again');
                    $scope.studentId = '';
                }
            );

        };
    };

    $scope.selectSport = function (item) {
        $scope.studentSportId = item.studentSportID;
    }

    function getPastCheckouts() {
        $scope.pastCheckouts = Checkout.getPastCheckoutResource.get({ studentId: $scope.studentId });
        $scope.pastCheckouts.$promise.then(
            function (result) {
                if ($filter('filter')($scope.pastCheckouts, function (d) { return (d.workoutStage.indexOf('pre') > -1 && d.workoutStage.indexOf('preAndPost') == -1); }).length > 0) {
                    $scope.pastPreCheckout = moment($filter('filter')($scope.pastCheckouts, function (d) { return (d.workoutStage.indexOf('pre') > -1 && d.workoutStage.indexOf('preAndPost') == -1); })[0].createDate).format('LT');
                }

                if ($filter('filter')($scope.pastCheckouts, function (d) { return d.workoutStage.indexOf('post') > -1; }).length > 0) {
                    $scope.pastPostCheckout = moment($filter('filter')($scope.pastCheckouts, function (d) { return d.workoutStage.indexOf('post') > -1; })[0].createDate).format('LT');
                }

                if ($filter('filter')($scope.pastCheckouts, function (d) { return d.workoutStage.indexOf('preAndPost') > -1; }).length > 0) {
                    $scope.pastPreAndPostCheckout = moment($filter('filter')($scope.pastCheckouts, function (d) { return d.workoutStage.indexOf('preAndPost') > -1; })[0].createDate).format('LT');
                }
            })

    }

    function _workoutStageComplete() {
        $('#workoutStageModal').modal('hide');

        //var sportDates = SportCode.resource.get({ id: $scope.sportCodeId });

        //sportDates.$promise.then(function (data) {
        //    var today = moment().format();


        //    if ((today > data.inSeasonStartDate) && (today < data.inSeasonEndDate)) {
        //        $scope.fullFuelStage = 'in-' + $scope.fuelStage;
        //    }
        //    else if ((today > data.offSeasonStartDate) && (today < data.offSeasonEndDate)) {
        //        $scope.fullFuelStage = 'out-' + $scope.fuelStage;
        //    }


        //    $scope.choices = DefaultChoice.getBasketResource.get({
        //        sportId: $scope.sportCodeId,
        //        stageId: $scope.fullFuelStage
        //    });

        //    $scope.choices.$promise.then(function () {
        //        choicesCopy = angular.copy($scope.choices);
        //    })


        //})

        var choices = Choice.getAllChoices.get();

        choices.$promise.then(function (data) {
            $scope.choices = data;
        }, function (error) {
            alert("There was an error loading your choices.");
        });


        $('#checkoutModal').modal('show');

    }

    function _workoutStageCancel() {
        $scope.clear();
        $('#workoutStageModal').modal('hide');
    }

    $scope.setSid = function (num) {
        $scope.studentId = num;
        checkStudentId();
    }

    $scope.addChoice = function (choice) {
        var currentIndex = $scope.choices.indexOf(choice);
        $scope.order.push(choice);
        $scope.choices.splice(currentIndex, 1);
    }

    $scope.deleteChoice = function (choice) {
        var currentIndex = $scope.order.indexOf(choice);
        $scope.order.splice(currentIndex, 1);
        $scope.choices.push(choice);
    }

    $scope.checkout = function () {
        if ($scope.order.length < 1) {
            alert('You do not have any fuel items selected. Please select an item before checking out.');
        }
        else {
            var newCheckout = new Checkout.resource();

            newCheckout.createDate = moment().format();
            newCheckout.studentSportID = $scope.studentSportId;
            newCheckout.workoutStage = $scope.fullFuelStage
            newCheckout.$save().then(function () {
                for (var i = 0; i < $scope.order.length; i++) {
                    var newCheckoutChoice = new checkoutChoiceRepository();
                    newCheckoutChoice.choiceID = $scope.order[i].choiceID;
                    newCheckoutChoice.checkoutID = newCheckout.checkoutID;
                    newCheckoutChoice.$save();
                }

                $scope.clear();
                $('#checkoutModal').modal('hide');
            });
        }
    }


    function _cancelCheckout() {
        $scope.clear();
        $('#checkoutModal').modal('hide');


    }

    $scope.clear = function () {
        $scope.studentId = '';
        $scope.sportCodeId = '';
        $scope.order = [];

        $scope.studentSportId = null;
        $scope.fuelStage = null;
        $scope.pastCheckouts = null;
        $scope.pastPreCheckout = null;
        $scope.pastPostCheckout = null;
        $scope.pastPreAndPostCheckout = null;

    }

    $scope.clearChoices = function () {
        $scope.order = [];
        $scope.choices = angular.copy(choicesCopy);

    }

});

fs.controller('CheckoutListController', function ($scope, Checkout, Category, DefaultChoice, checkoutChoiceRepository) {
    //todo: is this necessary to run the the query thru network here?
    Category.refresh();
    Checkout.refresh();
    //Checkout.getArchives();

    $scope.categories = Category.categories;
    $scope.checkouts = Checkout.checkouts;

    $scope.order = [];

    $scope.editArchives = function () {
        refreshArchiveData();
        $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);
        $('#archivesModal').modal('show');
    }

    $scope.setArchive = function (checkout) {
        Checkout.currentCheckout = checkout;
        Checkout.currentCheckoutIndex = Checkout.checkouts.indexOf(checkout);
        Checkout.currentCheckout.isArchived = true;
        Checkout.currentCheckout.archiveDate = moment().format()
        Checkout.resource.update({ id: checkout.checkoutID }, Checkout.currentCheckout)
            .$promise.then(function () {
                Checkout.checkouts[Checkout.currentCheckoutIndex] = checkout;
            });
    }

    $scope.setUnarchive = function (event, row) {
        event.stopPropagation();
        var archive = row.entity;
        var deleteIndex = $scope.myData.indexOf(archive);
        //alert("Here I need to know which button was selected " + row.entity.fullName)

        Checkout.currentCheckout = archive;
        Checkout.currentCheckoutIndex = Checkout.checkouts.indexOf(archive);
        Checkout.currentCheckout.isArchived = false;
        Checkout.currentCheckout.archiveDate = null;
        Checkout.resource.update({ id: archive.checkoutID }, Checkout.currentCheckout)
            .$promise.then(function () {
                Checkout.checkouts[Checkout.currentCheckoutIndex] = archive;
                $scope.myData.splice(deleteIndex, 1);
                $scope.totalServerItems = $scope.myData.length;
            });
    }

    //todo: remove this duplicate function
    $scope.addChoice = function (choice) {

        var newCheckoutChoice = new checkoutChoiceRepository();
        newCheckoutChoice.choiceID = choice.choiceID;
        newCheckoutChoice.checkoutID = Checkout.currentCheckout.checkoutID;
        newCheckoutChoice.$save().then(function (data) {
            //todo: seems so complicated and cumbersome below...
            Checkout.checkouts[Checkout.currentCheckoutIndex] = Checkout.resource.get({ id: Checkout.currentCheckout.checkoutID }, function () {
                $scope.order = Checkout.checkouts[Checkout.currentCheckoutIndex].checkoutChoices;
            });
            var currentIndex = $scope.choices.indexOf(choice);
            $scope.choices.splice(currentIndex, 1);
        });

    }

    $scope.clearChoices = function () {

        for (var i = 0; i < $scope.order.length; i++) {
            checkoutChoiceRepository.remove({ id: $scope.order[i].checkoutChoiceID })
        }

        $scope.checkouts[Checkout.currentCheckoutIndex].checkoutChoices = [];
        $scope.choices = angular.copy(choicesCopy);
        $scope.order = [];
        //    $scope.choices[i] = Checkout.checkouts[Checkout.currentCheckoutIndex].choices[i].choice;
    }

    $scope.deleteChoice = function (choice) {
        Checkout.currentChoiceIndex = $scope.order.indexOf(choice);
        checkoutChoiceRepository.remove({ id: choice.checkoutChoiceID }, function () {
            //todo: these three commands seem cumbersome, as well
            Checkout.currentCheckout.checkoutChoices.splice(Checkout.currentChoiceIndex, 1);
            $scope.checkouts[Checkout.currentCheckoutIndex].checkoutChoices.splice(Checkout.currentChoiceIndex, 1);
            //$scope.choices[Checkout.currentChoiceIndex].choices.splice(choice, 1);
            var currentIndex = $scope.order.indexOf(choice);
            $scope.choices.push(choice);
        });

    }

    $scope.editCheckout = function (checkout) {

        $scope.currentCheckout = Checkout.resource.get({ id: checkout.checkoutID });

        $scope.currentCheckout.$promise.then(function () {
            Checkout.currentCheckout = $scope.currentCheckout;
            Checkout.currentCheckoutIndex = $scope.checkouts.indexOf(checkout);

            $scope.choices = DefaultChoice.getBasketResource.get({
                sportId: Checkout.currentCheckout.studentSport.sportCode.sportCodeID,
                stageId: Checkout.currentCheckout.workoutStage
            });

            $scope.choices.$promise.then(function () {
                choicesCopy = angular.copy($scope.choices);
            })

            $scope.order = Checkout.checkouts[Checkout.currentCheckoutIndex].checkoutChoices;
            $('#editModal').modal('show');
        })
    }

    $scope.reset = function () {
        $scope.order = [];
        $scope.choices = [];
    }

    //
    //archive options below
    //

    var tmpData = [],
    tmpObj = {},
    data = [];

    $scope.editableInPopup = '<button type="button" class="btn btn-link" ng-click="setUnarchive($event, row)"><span class="glyphicon glyphicon-plus"></span></button>'

    function refreshArchiveData() {
        data = [];
        tmpData = Checkout.checkouts.filter(function (archives) {
            return archives.isArchived == true;
        });

        tmpData.sort(function (a, b) {
            return a['archiveDate'] < b['archiveDate'] ? 1 : -1;
        })

        //for (var i = 0; i < tmpData.length; i++) {
        //    for (var key in tmpData[i]) {
        //        if (key == 'fullName' || key == 'createDate') {
        //            tmpObj[key] = tmpData[i][key];
        //        }
        //    }
        //    data.push(tmpObj);
        //    tmpObj = {};
        //};

        data = tmpData;

    }


    $scope.filterOptions = {
        filterText: "",
        useExternalFilter: true
    };
    $scope.totalServerItems = 0;
    $scope.pagingOptions = {
        pageSizes: [10, 20],
        pageSize: 10,
        currentPage: 1
    };
    $scope.setPagingData = function (data, page, pageSize) {
        var pagedData = data.slice((page - 1) * pageSize, page * pageSize);
        $scope.myData = pagedData;
        $scope.totalServerItems = data.length;
        if (!$scope.$$phase) {
            $scope.$apply();
        }
    };

    $scope.getPagedDataAsync = function (pageSize, page, searchText) {
        setTimeout(function () {

            if (searchText) {
                var ft = searchText.toLowerCase();
                $http.get('largeLoad.json').success(function (largeLoad) {
                    data = largeLoad.filter(function (item) {
                        return JSON.stringify(item).toLowerCase().indexOf(ft) != -1;
                    });
                    $scope.setPagingData(data, page, pageSize);
                });
            } else {
                //$http.get('largeLoad.json').success(function (largeLoad) {
                $scope.setPagingData(data, page, pageSize);
                //});
            }
        }, 100);
    };

    $scope.$watch('pagingOptions', function (newVal, oldVal) {
        if (newVal !== oldVal && newVal.currentPage !== oldVal.currentPage) {
            $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage, $scope.filterOptions.filterText);
        }
    }, true);
    $scope.$watch('filterOptions', function (newVal, oldVal) {
        if (newVal !== oldVal) {
            $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage, $scope.filterOptions.filterText);
        }
    }, true);

    $scope.gridOptions = {
        data: 'myData',
        enablePaging: true,
        enableSorting: false,
        showFooter: true,
        totalServerItems: 'totalServerItems',
        pagingOptions: $scope.pagingOptions,
        filterOptions: $scope.filterOptions,
        columnDefs: [{ field: 'studentSport.fullName', displayName: 'Name' },
            { field: 'createDate', displayName: 'Time Order Created', cellFilter: "date: 'hh:mm a'" },
            { field: 'archiveDate', displayName: 'Time Order Archived', cellFilter: "date: 'hh:mm a'" },
                        { displayName: 'Unarchive', cellTemplate: $scope.editableInPopup }]
    };

});