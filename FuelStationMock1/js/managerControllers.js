﻿
fs.controller("FuelController", function ($scope, Category, Choice, FSConfig, $timeout) {

    // Snack Limit Settings
    $scope.snackLimitSaved = false;
    FSConfig.snackLimits.get()
        .$promise.then(function (result) {
            $scope.snackLimits = result;
        }, function (error) {
            console.log(error)
        });

    $scope.onSubmitSnackLimits = function () {
        FSConfig.snackLimits.update($scope.snackLimits)
            .$promise.then(function (response) {
                $timeout(function () {
                    $scope.snackLimitSaved = true;
                }, 500);
            }, function (error) {
                console.error("Error saving snack limits: ", error);
            });
    }






    //todo: service
    Category.refresh();
    $scope.categories = Category.categories;
    $scope.choices = [];

    // NEW

    init();

    function init() {
        Choice.getAllChoices.query(function (data) {
            $scope.allChoices = data;
        });
    }

    $scope.order = function (predicate) {
        $scope.reverse = ($scope.predicate === predicate) ? !$scope.reverse : false;
        $scope.predicate = predicate;
    };

    $scope.editChoice = function (choice) {
        choice.editMode = true;
    }

    $scope.saveChoice = function (choice, choiceID) {

        if (choiceID) {
            Choice.resource.update(choice, onUpdateSuccess, genericErrorAlert);
        }
        else if (!choiceID) {
            choice.categoryID = 2;
            Choice.resource.save(choice, onPostSuccess, genericErrorAlert);
        }

        function onPostSuccess(data) {
            choice.choiceID = data.choiceID;
            onUpdateSuccess();
        }

        function onUpdateSuccess() {
            choice.saved = true;
            choice.editMode = false;
        }
    }

    $scope.addChoice2 = function () {
        $scope.allChoices.unshift(
            {
                editMode: true,
                newItem: true
            });
    }

    $scope.deleteChoice2 = function (choice) {
        var confirmDelete = confirm("Are you sure you want to delete this item?");

        if (confirmDelete) {
            var currentChoiceIndex = $scope.allChoices.indexOf(choice);

            if (choice.choiceID) {
                Choice.resource.delete({ id: choice.choiceID }, onDeleteSuccess, genericErrorAlert);
            }
            else {
                onDeleteSuccess();
            }

            function onDeleteSuccess() {
                $scope.allChoices.splice(currentChoiceIndex, 1);
            }
        }
    }

    function alphabetize(a, b) {
        if (a.name < b.name)
            return -1;
        else if (a.name > b.name)
            return 1;
        else
            return 0;
    }



    function genericErrorAlert() {
        alert("There was an error. Please try again.");
    }




    //local vars
    $scope.choiceExists = false;
    $scope.categoryName = "";

    $scope.addCategory = function (category) {
        var newCategoryResource = new Category.resource;
        newCategoryResource.name = category.name;
        newCategoryResource.description = category.description;
        newCategoryResource.$save().then(function (data) {
            $scope.categories.push(data);
        });
        $('#addModal').modal('hide');

    }

    $scope.deleteCategory = function (category) {
        $scope.edit_category_idx = $scope.categories.indexOf(category);
        $scope.edit_category = Category.resource.delete({ id: category.categoryID })
            .$promise.then(function () {
                $scope.categories.splice($scope.edit_category_idx, 1)
            });
    }

    $scope.editCategory = function (category) {
        $scope.category = Category.resource.get({ id: category.categoryID }, function (data) {
            Category.currentCategory = $scope.category;
            Category.currentCategoryIndex = $scope.categories.indexOf(category);
            $scope.choices = Category.currentCategory.choices;
        });
    }

    $scope.addChoice = function () {
        newChoiceResource = new Choice.resource;
        newChoiceResource.name = 'Insert Choice Name Here';
        newChoiceResource.categoryID = Category.currentCategory.categoryID;

        newChoiceResource.$save().then(function () {
            $scope.choices.push(newChoiceResource);
        });
    }

    $scope.deleteChoice = function (choice) {
        Choice.currentChoiceIndex = $scope.choices.indexOf(choice);
        Choice.resource.delete({ id: choice.choiceID })
            .$promise.then(function () {
                $scope.choices.splice(Choice.currentChoiceIndex, 1);
            });
    }

    $scope.save = function () {
        Category.resource.update({ id: Category.currentCategory.categoryID }, Category.currentCategory)
            .$promise.then(function () {
                $scope.categories[Category.currentCategoryIndex].name = Category.currentCategory.name;
                $scope.categories[Category.currentCategoryIndex].description = Category.currentCategory.description;

                for (var i = 0; i < $scope.choices.length; i++) {
                    Choice.resource.update({ id: $scope.choices[i].choiceID }, $scope.choices[i])
                    .$promise.then(function () {
                        $scope.categories[Category.currentCategoryIndex].choices = $scope.choices;
                    })
                }
                $('#editModal').modal('hide');
            });
    }



});

fs.controller('ReportController', function ($scope, Report) {

    $scope.myData = Report.dailyCheckouts;

    $scope.gridOptions = {
        data: 'myData',
        enableSorting: false,
        columnDefs: [{ field: 'firstname', displayName: 'First Name' },
            { field: 'lastname', displayName: 'Last Name' },
            { field: 'choiceName', displayName: 'Choice' },
            { field: 'createDate', displayName: 'Time Order Created', cellFilter: "date: 'hh:mm a'" },
            { field: 'archiveDate', displayName: 'Time Order Archived', cellFilter: "date: 'hh:mm a'" }]
    }
    function JSON2CSV(objArray) {
        var array = typeof objArray != 'object' ? JSON.parse(objArray) : objArray;

        var str = '';
        var line = '';

        if ($("#labels").is(':checked')) {
            var head = array[0];
            if ($("#quote").is(':checked')) {
                for (var index in array[0]) {
                    var value = index + "";
                    line += '"' + value.replace(/"/g, '""') + '",';
                }
            } else {
                for (var index in array[0]) {
                    line += index + ',';
                }
            }

            line = line.slice(0, -1);
            str += line + '\r\n';
        }

        for (var i = 0; i < array.length; i++) {
            var line = '';

            if ($("#quote").is(':checked')) {
                for (var index in array[i]) {
                    var value = array[i][index] + "";
                    line += '"' + value.replace(/"/g, '""') + '",';
                }
            } else {
                for (var index in array[i]) {
                    if (index.charAt(0) != '$')
                        line += array[i][index] + ',';
                }
            }

            line = line.slice(0, -1);
            str += line + '\r\n';
        }
        return str;

    }

    $("#download").click(function () {
        var json = $scope.myData;

        var csv = JSON2CSV(json);
        var uriFile = "data:text/csv;charset=utf-8," + escape(csv)
        var link = document.createElement("a");
        link.setAttribute("href", uriFile);
        link.setAttribute("download", "my_data.csv");

        link.click();
        //    window.open("data:text/csv;charset=utf-8," + escape(csv))
    });

});

fs.controller('CategoryController', function ($scope, Category) {

    //$scope.category = Category.get({ id: category.fuelCategoryID }, function (data) { });
});

//todo: remove Athlete, checkoutChoiceRepository
fs.controller('DefaultChoicesController', function ($scope, DefaultChoice, SportCode, Checkout, Category) {
    $scope.setActiveBasket = _setActiveBasket;
    $scope.addChoice = _addChoice;
    $scope.selectSport = _selectSport;
    $scope.refresh = _refresh;
    $scope.clearChoices = _clearChoices;
    $scope.clearGUI = _clearGUI;
    $scope.deleteChoice = _deleteChoice;
    $scope.loadDefault = _loadDefault;
    $scope.saveDates = _saveDates;
    $scope.showDatesModal = _showDatesModal;

    Checkout.refresh();
    Category.refresh();
    SportCode.refresh();

    $scope.categories = Category.categories;
    $scope.checkouts = Checkout.checkouts;
    $scope.sportCodes = SportCode.sportCodes;
    $scope.sportCodeId = '';

    $scope.formData = {};
    $scope.formData.radio = 'default';

    $scope.baskets = [
        {
            'isActive': false,
            "basketName": "in-pre",
            'description1': 'In Season',
            'description2': 'Pre-Workout',
            'choices': []
        },
        {
            'isActive': false,
            "basketName": "in-post",
            'description1': 'In Season',
            'description2': 'Post-Workout',
            'choices': []
        },
        {
            'isActive': false,
            "basketName": "out-pre",
            'description1': 'Off Season',
            'description2': 'Pre-Workout',
            'choices': []
        },
        {
            'isActive': false,
            'basketName': "out-post",
            'description1': 'Off Season',
            'description2': 'Post-Workout',
            'choices': []
        }
    ];

    $scope.refresh('DEF');

    function _setActiveBasket(index) {
        if ($scope.formData.currentBasket != null)
            $scope.baskets[$scope.formData.currentBasket].isActive = false;
        $scope.formData.currentBasket = index;
        $scope.baskets[index].isActive = true;

    };

    function _addChoice(choice) {
        //todo: error on click if  currentBasket is not set
        if ($scope.formData.currentBasket == null) {
            alert("First, click 'Set Active' on one of the baskets on the right.")
            return;
        }

        var newChoice = new DefaultChoice.resource();

        newChoice.SportCodeID = $scope.formData.sportCodeId;
        newChoice.ChoiceID = choice.choiceID;
        newChoice.FuelStage = $scope.baskets[$scope.formData.currentBasket].basketName;

        newChoice.$save().then(function (data) {
            var choiceObj = data;
            choiceObj.choice = choice;
            $scope.baskets[$scope.formData.currentBasket].choices.push(choiceObj);
        })
    };

    function _loadDefault() {
        DefaultChoice.loadDefaultResource.save({ sportId: $scope.formData.sportCodeId })
            .$promise.then(function () {
                _refresh($scope.formData.sportCodeId);
            });
    }
    function _selectSport(option) {
        $scope.formData.sportDescription = option.sportDescription;
        var sportCode = SportCode.resource.get({ id: option.sportCodeID });
        sportCode.$promise.then(function (data) {
            SportCode.currentSportCode = data;
            $scope.currentSportCode = data;
        })

        $scope.refresh(option.sportCodeID);
    };

    $scope.open = function ($event, openId) {
        $event.preventDefault();
        $event.stopPropagation();

        $scope[openId] = true;
    };

    function _showDatesModal() {
        $scope.currentSportCodeModal = angular.copy($scope.currentSportCode);
        $('#seasonDatesModal').modal('show');
        //seasonDatesModal
    }

    function _saveDates() {
        //copy to database... then promise run code below
        $scope.currentSportCode = angular.copy($scope.currentSportCodeModal);
        SportCode.resource.update({ id: $scope.currentSportCode.sportCodeID }, $scope.currentSportCode)
            .$promise.then(function () {
                $('#seasonDatesModal').modal('hide');
            });

    }

    function _refresh(option) {
        $scope.formData.sportCodeId = option;
        if (option == 'DEF')
            $scope.formData.sportDescription = "Select Sport";
        $scope.clearGUI;
        for (var i = 0; i < $scope.baskets.length; i++) {
            $scope.baskets[i].choices = DefaultChoice.getBasketResource.get({
                sportId: $scope.formData.sportCodeId,
                stageId: $scope.baskets[i].basketName
            });
            $scope.baskets[i].isActive = false;
        }
    };

    function _clearChoices(index) {
        DefaultChoice.deleteBasketResource.save({
            stageId: $scope.baskets[index].basketName,
            sportId: $scope.formData.sportCodeId
        }).$promise.then(function () {

            $scope.baskets[index].choices = [];
        });
    };

    function _clearGUI() {
        for (var i = 0; i < $scope.baskets.length; i++) {
            $scope.baskets[i].isActive = false;
            $scope.baskets[i].choices = [];
        }
    };

    function _deleteChoice(choice) {
        var currentIndex = $scope.baskets[$scope.formData.currentBasket].choices.indexOf(choice);
        var deleteChoice = new DefaultChoice.resource();
        deleteChoice.$remove({ id: choice.defaultChoiceID }).then(function (data) {
            $scope.baskets[$scope.formData.currentBasket].choices.splice(currentIndex, 1);
        });
    }


})