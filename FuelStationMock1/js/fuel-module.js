﻿/// <reference path="C:\inetpub\FuelStationMock1\FuelStationMock1\Views/Shared/_Layout.cshtml" />
var fs = angular.module("fs", ['ng', 'ngRoute', 'ngResource', 'ngTouch', 'ngGrid', 'ui.bootstrap'])
    .config(function ($routeProvider, $locationProvider) {
        $routeProvider.when('/manager/categories', { templateUrl: '/templates/categories.html', controller: 'FuelController' });
        $routeProvider.when('/manager/allSnacks', { templateUrl: '/templates/snacks.html', controller: 'FuelController' });
        $routeProvider.when('/manager/settings', { templateUrl: '/templates/settings.html', controller: 'FuelController' });
        $routeProvider.when('/manager/dailyReport', { templateUrl: '/templates/dailyReport.html', controller: 'ReportController' });
        $routeProvider.when('/manager/weeklyReport', { templateUrl: '/templates/weeklyReport.html', controller: 'ReportController' });
        $routeProvider.when('/manager/defaultChoices', { templateUrl: '/templates/defaultChoices.html', controller: 'DefaultChoicesController' });
        //$routeProvider.when('/Fuel/Choices', { templateUrl: '/templates/choices.html', controller: 'ChoicesController' });
        $locationProvider.html5Mode(true);

    })
    .config(function (datepickerConfig) {
        datepickerConfig.showWeeks = false;
    });

fs.value('fuelItems', {});

fs.filter("newOnTop", function () {
    return function (array, key) {
        if (!angular.isArray(array)) return;
        var present = array.filter(function (item) {
            return item[key];
        });
        var empty = array.filter(function (item) {
            return !item[key]
        });
        return present.concat(empty);
    };
});