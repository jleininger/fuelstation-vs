﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using FuelStationMock1.Models.Rev2;
using System.Web.Http.Cors;

namespace FuelStationMock1.Controllers
{
    //[EnableCors(origins: "http://localhost:8100", headers: "*", methods: "*")]
    public class SportCodesController : ApiController
    {
        private FsContext db = new FsContext();

        // GET: api/SportCodes
        public IQueryable<SportCode> Get()
        {
            return db.SportCodes;
        }

        // GET: api/SportCodes
        [Route("api/SportCodes/GetRealSportCodes")]
        [HttpGet]
        public IQueryable<SportCode> GetRealSportCodes()
        {
            return db.SportCodes.Where(s => s.SportDescription.IndexOf("Generic") == -1);
        }

        // GET: api/SportCodes/5
        [Route("api/SportCodes/{id}")]
        [ResponseType(typeof(SportCode))]
        public async Task<IHttpActionResult> Get(string id)
        {
            SportCode sportCode = await db.SportCodes.FindAsync(id);
            if (sportCode == null)
            {
                return NotFound();
            }

            return Ok(sportCode);
        }

        // PUT: api/SportCodes/5
        [Route("api/SportCodes/{id}")]
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> Put(string id, SportCode sportCode)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != sportCode.SportCodeID)
            {
                return BadRequest();
            }

            db.Entry(sportCode).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!SportCodeExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/SportCodes
        [ResponseType(typeof(SportCode))]
        public async Task<IHttpActionResult> Post(SportCode sportCode)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.SportCodes.Add(sportCode);

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (SportCodeExists(sportCode.SportCodeID))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = sportCode.SportCodeID }, sportCode);
        }

        // DELETE: api/SportCodes/5
        [ResponseType(typeof(SportCode))]
        public async Task<IHttpActionResult> Delete(string id)
        {
            SportCode sportCode = await db.SportCodes.FindAsync(id);
            if (sportCode == null)
            {
                return NotFound();
            }

            db.SportCodes.Remove(sportCode);
            await db.SaveChangesAsync();

            return Ok(sportCode);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool SportCodeExists(string id)
        {
            return db.SportCodes.Count(e => e.SportCodeID == id) > 0;
        }
    }
}