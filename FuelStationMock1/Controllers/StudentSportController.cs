﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using FuelStationMock1.Models.Rev2;
using System.Web.Http.Cors;

namespace FuelStationMock1.Controllers
{
    [EnableCors(origins: "http://localhost:8100", headers: "*", methods: "*")]
    public class StudentSportController : ApiController
    {
        private FsContext db = new FsContext();

        // GET: api/StudentSport
        public IQueryable<StudentSport> GetStudentSports()
        {
            return db.StudentSports;
        }

        // GET: api/StudentSport/5
        [ResponseType(typeof(StudentSport))]
        public async Task<IHttpActionResult> GetStudentSport(string id)
        {
            List<StudentSport> studentSports = await db.StudentSports.Where(s => s.schoolsidnumber == id).ToListAsync();
            if (studentSports == null)
            {
                return NotFound();
            }

            return Ok(studentSports);
        }

        // PUT: api/StudentSport/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutStudentSport(string schoolsidnumber, StudentSport studentSport)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (schoolsidnumber != studentSport.schoolsidnumber)
            {
                return BadRequest();
            }

            db.Entry(studentSport).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!StudentSportExists(schoolsidnumber))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/StudentSport
        [ResponseType(typeof(StudentSport))]
        public async Task<IHttpActionResult> PostStudentSport(StudentSport studentSport)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.StudentSports.Add(studentSport);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = studentSport.StudentSportID }, studentSport);
        }

        // DELETE: api/StudentSport/5
        [ResponseType(typeof(StudentSport))]
        public async Task<IHttpActionResult> DeleteStudentSport(int id)
        {
            StudentSport studentSport = await db.StudentSports.FindAsync(id);
            if (studentSport == null)
            {
                return NotFound();
            }

            db.StudentSports.Remove(studentSport);
            await db.SaveChangesAsync();

            return Ok(studentSport);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool StudentSportExists(string schoolsidnumber)
        {
            return db.StudentSports.Count(e => e.schoolsidnumber == schoolsidnumber) > 0;
        }
    }
}