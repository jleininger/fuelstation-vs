﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using FuelStationMock1.Models.Rev2;
using System.Web.Http.Cors;

namespace FuelStationMock1.Controllers
{
    //[EnableCors(origins: "http://localhost:8100", headers: "*", methods: "*")]
    public class DefaultChoicesController : ApiController
    {
        private FsContext db = new FsContext();

        // GET: api/DefaultChoices
        public IQueryable<DefaultChoice> Get()
        {
            return db.DefaultChoices;
        }

        // GET: api/DefaultChoicesByFuelStage
        [Route("api/DefaultChoices/{sportId}/stage/{stageId}")]
        [HttpGet]
        public async Task<IHttpActionResult> GetDefaultChoicesByFuelStage(string sportId, string stageId)
        {
            var returnObject = new List<DefaultChoice>();

            if (stageId.Contains("preAndPost"))
            {
                var preAndPost = new[] { "pre", "post" };
                string season;

                if (stageId.Contains('-'))
                {
                    season = stageId.Split('-')[0];
                    returnObject = await db.DefaultChoices.Where(c => (c.FuelStage == season + "-pre" || c.FuelStage == season + "-post") && sportId == c.SportCodeID).ToListAsync();
                }

                
                
            }
            else
            {
                returnObject = await db.DefaultChoices.Where(c => c.FuelStage == stageId && c.SportCodeID == sportId).ToListAsync();
            }

            if (returnObject == null)
            {
                return NotFound();

            }

            return Ok(returnObject);
        }

        [Route("api/DefaultChoices/SetDefaultChoicesByTemplate/{sportId}")]
        [HttpPost]
        public async Task<IHttpActionResult> SetDefaultChoicesByTemplate(string sportId)
        {
            db.DefaultChoices.RemoveRange(db.DefaultChoices.Where(x => x.SportCodeID == sportId));
            await db.SaveChangesAsync();

            var rows = await db.DefaultChoices.Where(c => c.SportCodeID == "DEF").ToListAsync();
            foreach (var row in rows)
            {
                row.SportCodeID = sportId;
            }

            rows.ForEach(r => db.DefaultChoices.Add(r));
            await db.SaveChangesAsync();

            return Ok();
        }

        [Route("api/DefaultChoices/DeleteDefaultChoicesByFuelStage/{sportId}/stage/{stageId}")]
        [HttpPost]
        public async Task<IHttpActionResult> DeleteDefaultChoicesByFuelStage(string sportId, string stageId)
        {

            db.DefaultChoices.RemoveRange(db.DefaultChoices.Where(x => x.FuelStage == stageId && x.SportCodeID == sportId));

            await db.SaveChangesAsync();

            return Ok();
        }

        // GET: api/DefaultChoices/5
        [ResponseType(typeof(DefaultChoice))]
        public async Task<IHttpActionResult> Get(int id)
        {
            DefaultChoice defaultChoice = await db.DefaultChoices.FindAsync(id);
            if (defaultChoice == null)
            {
                return NotFound();
            }

            return Ok(defaultChoice);
        }

        // PUT: api/DefaultChoices/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> Put(int id, DefaultChoice defaultChoice)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != defaultChoice.DefaultChoiceID)
            {
                return BadRequest();
            }

            db.Entry(defaultChoice).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!DefaultChoiceExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/DefaultChoices
        [ResponseType(typeof(DefaultChoice))]
        public async Task<IHttpActionResult> Post(DefaultChoice defaultChoice)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.DefaultChoices.Add(defaultChoice);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApiPost", new { id = defaultChoice.DefaultChoiceID }, defaultChoice);
        }

        // DELETE: api/DefaultChoices/5
        [ResponseType(typeof(DefaultChoice))]
        public async Task<IHttpActionResult> Delete(int id)
        {
            DefaultChoice defaultChoice = await db.DefaultChoices.FindAsync(id);
            if (defaultChoice == null)
            {
                return NotFound();
            }

            db.DefaultChoices.Remove(defaultChoice);
            await db.SaveChangesAsync();

            return Ok(defaultChoice);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool DefaultChoiceExists(int id)
        {
            return db.DefaultChoices.Count(e => e.DefaultChoiceID == id) > 0;
        }
    }
}