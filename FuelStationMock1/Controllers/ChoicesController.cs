﻿using FuelStationMock1.Models.Rev2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace FuelStationMock1.Controllers
{
    public class ChoicesController : ApiController
    {
        private FsContext db = new FsContext();

        // GET: api/DefaultChoices
        public IQueryable<Choice> Get()
        {
            return db.Choices;
        }
    }
}
