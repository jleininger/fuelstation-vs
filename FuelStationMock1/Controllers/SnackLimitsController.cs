﻿using FuelStationMock1.Models.Rev2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;

namespace FuelStationMock1.Controllers
{
    public class SnackLimitsController : ApiController
    {
        private FsContext db = new FsContext();

        // GET: api/SnackLimits/000000000
        [ResponseType(typeof(SnackLimit))]
        public IHttpActionResult Get(string id)
        {
            // Get list of sports student is in
            var listOfSports = db.StudentSports.Where(s => s.schoolsidnumber == id).Select(r => r.SportCodeID);

            if (listOfSports == null)
            {
                return NotFound();
            }

            // Get snack limits for each sport
            var limits = db.SnackLimits.Where(s => listOfSports.Contains(s.SportCodeId));

            // Get highest limit
            var maxLimit = limits.AsEnumerable().Aggregate((i, j) => i.DaySnackLimit > j.DaySnackLimit ? i : j);

            return Ok(maxLimit);
        }

        // GET: api/SnackLimits
        public IQueryable<SnackLimit> Get()
        {
            return db.SnackLimits.OrderBy(a => a.Sport.SportDescription);
        }

        // Put: api/SnackLimits/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> Put(int id, SnackLimit snackLimits)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != snackLimits.Id)
            {
                return BadRequest();
            }

            db.Entry(snackLimits).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!SnackLimitExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        private bool SnackLimitExists(int id)
        {
            return db.SnackLimits.Count(e => e.Id == id) > 0;
        }
    }
}
