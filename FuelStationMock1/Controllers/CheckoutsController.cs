﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using FuelStationMock1.Models.Rev2;
using System.Web.Http.Cors;
using FuelStationMock1.Services;
using FuelStationMock1.Models;
using System.Data.Entity.Core.Objects;

namespace FuelStationMock1.Controllers
{
    //[EnableCors(origins: "http://localhost:8100", headers: "*", methods: "*")]
    public class CheckoutsController : ApiController
    {
        private FsContext db = new FsContext();

        // GET: api/Checkouts
        public IQueryable<Checkout> Get()
        {
            //var checkouts = db.Checkouts.Where(f => f.isArchived == false).ToList();
            //return db.Checkouts.Where(f => f.isArchived == false);
            return db.Checkouts.Where(f => f.CreateDate > DateTime.Today);
        }

        [Route("api/Checkouts/Archived"),HttpGet]
        public IQueryable<Checkout> GetArchived()
        {
            return db.Checkouts.Where(f => f.CreateDate > DateTime.Today && f.isArchived == true)
                .Include(x => x.StudentSport)
                .Include(x => x.StudentSport.SportCode)
                .Include(x => x.CheckoutChoices)
                .Include(x => x.CheckoutChoices.Select(y => y.Choice));
        }

        [Route("api/Checkouts/Unarchived"), HttpGet]
        public IQueryable<Checkout> GetUnarchived()
        {
            return db.Checkouts.Where(f => f.CreateDate > DateTime.Today && f.isArchived == false)
                .Include(x => x.StudentSport)
                .Include(x => x.StudentSport.SportCode)
                .Include(x => x.CheckoutChoices)
                .Include(x => x.CheckoutChoices.Select(y => y.Choice));
        }


        // GET: api/GetDailyCheckouts
        [HttpGet]
        public async Task<IHttpActionResult> GetDailyCheckouts()
        {
            var returnObject = await db.Checkouts.Join(db.CheckoutChoices,
                a => a.CheckoutID,
                b => b.CheckoutID,
                (a, b) => new
                {
                    a.StudentSport.firstname,
                    a.StudentSport.lastname,
                    ChoiceName = b.Choice.Name,
                    a.isArchived,
                    a.CreateDate,
                    a.ArchiveDate,
                    a.CheckoutID,
                    a.StudentSport.SportCodeID,
                })
                .Where(f => f.CreateDate > DateTime.Today)
                .ToListAsync();
            if (returnObject == null)
            {
                return NotFound();

            }
            return Ok(returnObject);
        }

        // GET: api/GetMonthCounts
        [Route("api/Checkouts/GetMonthCounts/Month/{month:int}/Year/{year}")]
        [HttpGet]
        public async Task<IHttpActionResult> GetMonthCounts(int month, int year)
        {
            var returnObject = await db.Checkouts.Join(db.CheckoutChoices,
                checkout => checkout.CheckoutID,
                checkoutChoice => checkoutChoice.CheckoutID,
                (checkout, checkoutChoice) => new
                {
                    checkoutChoice.ChoiceID,
                    checkout.CreateDate
                })
                .Join(db.Choices,
                checkout => checkout.ChoiceID,
                choice => choice.ChoiceID,
                (checkout, choice) => new
                {
                    checkout.CreateDate,
                    choice.Name
                })
                .Where(checkout => checkout.CreateDate.Month == month && checkout.CreateDate.Year == year)
                .GroupBy(c => c.Name)
                .Select(choice => new { Choice = choice.Key, Count = choice.Count()})
                .ToListAsync();
            
            if (returnObject == null)
            {
                return NotFound();

            }
            return Ok(returnObject);

        }


        // GET: api/Checkouts
        [Route("api/Checkouts/GetPastCheckouts/{studentId}")]
        [HttpGet]
        public async Task<IHttpActionResult> GetPastCheckouts(string studentId)
        {
            var returnObject = await db.Checkouts
                .Join(db.StudentSports, a => a.StudentSportID, b => b.StudentSportID,
                (a, b) => new
                {
                    a.WorkoutStage,
                    a.CreateDate,
                    b.schoolsidnumber
                }
                )
                .Select(f => new { f.CreateDate, f.WorkoutStage, f.schoolsidnumber })
                .Where(f => f.CreateDate > DateTime.Today && f.schoolsidnumber == studentId)
                .ToListAsync();
            if (returnObject == null)
            {
                return NotFound();

            }

            return Ok(returnObject);
        }

        // GET: api/Checkouts/CurrentMonthCheckoutCount/{studentId}
        [Route("api/Checkouts/CurrentMonthCheckoutCount/{studentId}")]
        [HttpGet]
        public async Task<IHttpActionResult> GetMonthCheckoutCount(string studentId)
        {
            var returnObject = await db.Checkouts
                .Join(db.StudentSports, a => a.StudentSportID, b => b.StudentSportID,
                (a, b) => new
                {
                    a.WorkoutStage,
                    a.CreateDate,
                    b.schoolsidnumber,
                    a.CheckoutChoices
                }
                )
                .Select(f => new { f.schoolsidnumber, f.CheckoutChoices, f.CreateDate })
                .Where(f => f.CreateDate.Month == DateTime.Now.Month &&
                    f.CreateDate.Year == DateTime.Now.Year &&
                    f.schoolsidnumber == studentId)
                .ToListAsync();
            if (returnObject == null)
            {
                return NotFound();

            }

            return Ok(returnObject);
        }

        // GET: api/Checkouts/GetTodaysCheckoutCount/{studentId}
        [Route("api/Checkouts/GetTodaysCheckoutCount/{studentId}")]
        [HttpGet]
        public async Task<IHttpActionResult> GetTodaysCheckoutCount(string studentId)
        {
            var returnObject = await db.Checkouts
                .Join(db.StudentSports, a => a.StudentSportID, b => b.StudentSportID,
                (a, b) => new
                {
                    a.WorkoutStage,
                    a.CreateDate,
                    b.schoolsidnumber,
                    a.CheckoutChoices
                }
                )
                .Select(f => new { f.schoolsidnumber, f.CheckoutChoices, f.CreateDate })
                .Where(f => f.CreateDate.Month == DateTime.Now.Month &&
                    f.CreateDate.Day == DateTime.Now.Day &&
                    f.CreateDate.Year == DateTime.Now.Year &&
                    f.schoolsidnumber == studentId)
                .ToListAsync();
            if (returnObject == null)
            {
                return NotFound();

            }

            return Ok(returnObject);
        }

        [Route("api/Checkouts/History/{studentSportID}")]
        [HttpGet]
        public HttpResponseMessage GetCheckoutHistory(int studentSportID)
        {
            CheckoutHistory response = new CheckoutHistory();

            response = CheckoutService.GetCheckoutHistory(studentSportID);

            return Request.CreateResponse(response);
        }

        // GET: api/Checkouts/Archives
        [HttpGet]
        public IQueryable<Checkout> Archives()
        {
            return db.Checkouts.Where(f => f.isArchived == true);
        }

        // GET: api/Checkouts/5
        [ResponseType(typeof(Checkout))]
        public async Task<IHttpActionResult> Get(int id)
        {
            Checkout checkout = await db.Checkouts.FindAsync(id);
            if (checkout == null)
            {
                return NotFound();
            }

            return Ok(checkout);
        }

        // PUT: api/Checkouts/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> Put(int id, Checkout checkout)
        {
            checkout.CheckoutChoices = null;
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != checkout.CheckoutID)
            {
                return BadRequest();
            }

            db.Entry(checkout).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CheckoutExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Checkouts
        [ResponseType(typeof(Checkout))]
        public async Task<IHttpActionResult> Post(Checkout checkout)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            int snackCount = 0;
            SnackLimit limits = GetSnackLimits(checkout.StudentSportID);

            foreach (var choice in checkout.CheckoutChoices)
            {
                if (choice.IsSnack == true)
                {
                    snackCount++;
                }
            }

            CheckoutHistory history = new CheckoutHistory();
            history = CheckoutService.GetCheckoutHistory(checkout.StudentSportID);

            if (snackCount + history.DaySnacksCount > limits.DaySnackLimit ||
                snackCount + history.MonthSnacksCount > limits.MonthSnackLimit)
            {
                HttpError err = new HttpError("Sorry, you have already exceeded your snack limits.");
                return Content(HttpStatusCode.BadRequest, err);
            }

            foreach (var choice in checkout.CheckoutChoices)
            {
                if (choice.Type == 1 && choice.IsSnack == false && history.DayPreCount == 1 ||
                    choice.Type == 2 && choice.IsSnack == false && history.DayPostCount == 1 ||
                    choice.Type == 3 && choice.IsSnack == false && history.DayHydrationCount == 1)
                {
                    HttpError err = new HttpError("Sorry, there was an error. Please try again.");
                    return Content(HttpStatusCode.BadRequest, err);
                }
            }

            db.Checkouts.Add(checkout);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApiPost", new { id = checkout.CheckoutID }, checkout);
        }

        // DELETE: api/Checkouts/5
        public async Task<IHttpActionResult> Delete(int id)
        {
            Checkout checkout = await db.Checkouts.FindAsync(id);
            if (checkout == null)
            {
                return NotFound();
            }

            db.Checkouts.Remove(checkout);
            await db.SaveChangesAsync();

            return Ok();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool CheckoutExists(int id)
        {
            return db.Checkouts.Count(e => e.CheckoutID == id) > 0;
        }

        private async Task<int> GetTodaysCount(int studentSportID)
        {
            var returnObject = await db.Checkouts
                .Join(db.StudentSports, co => co.StudentSportID, st => st.StudentSportID,
                (co, st) => new
                {
                    co,
                    st
                }
                )
                .Join(db.CheckoutChoices, co2 => co2.co.CheckoutID, cc => cc.CheckoutID,
                (co2, cc) => new
                {
                    co2,
                    cc
                })
                .Select(f => new { f.co2.co.CreateDate, f.co2.st.StudentSportID, f.cc.ChoiceID })
                .Where(f => f.CreateDate.Month == DateTime.Now.Month &&
                    f.CreateDate.Day == DateTime.Now.Day &&
                    f.CreateDate.Year == DateTime.Now.Year &&
                    f.StudentSportID == studentSportID)
                .ToListAsync();

            if (returnObject != null)
            {
                return returnObject.Count();
            }

            return 0;
        }

        private SnackLimit GetSnackLimits(int studentSportId)
        {
            SnackLimits limits = new SnackLimits();

            var sportCodeId = db.StudentSports
                .Where(x => x.StudentSportID == studentSportId)
                .Select(x => x.SportCodeID)
                .FirstOrDefault();

            var snackLimits = db.SnackLimits
                .Where(x => x.SportCodeId == sportCodeId)
                .FirstOrDefault();


             
            //var daySnackLimit = (from item in db.FuelStationConfig
            //                     where item.Description == "DaySnackLimit"
            //                     select item.Value).Single();

            //var monthSnackLimit = (from item in db.FuelStationConfig
            //                       where item.Description == "MonthSnackLimit"
            //                       select item.Value).Single();

            //limits.DaySnackLimit = Convert.ToInt32(daySnackLimit);
            //limits.MonthSnackLimit = Convert.ToInt32(monthSnackLimit);

            return snackLimits;
        }
    }
}