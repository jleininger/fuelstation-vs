﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using FuelStationMock1.Models.Rev2;
using System.Web.Http.Cors;
using FuelStationMock1.Services;
using FuelStationMock1.Models;
namespace FuelStationMock1.Controllers
{
    public class FuelStationConfigController : ApiController
    {
        private FsContext db = new FsContext();


        [Route("api/FuelStationConfig/SnackLimits"), HttpPut]
        public HttpResponseMessage UpdateSettings(SnackLimits model)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }

            try
            {
                FuelStationConfigService.UpdateFuelStationSettings(model);
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(e);
            }
        }

        [Route("api/FuelStationConfig/SnackLimits"), HttpGet]
        public  HttpResponseMessage GetSnackLimits()
        {
            SnackLimits limits = new SnackLimits();

            var daySnackLimit = (from item in db.FuelStationConfig
                                where item.Description == "DaySnackLimit"
                                select item.Value).Single();

            var monthSnackLimit = (from item in db.FuelStationConfig
                                  where item.Description == "MonthSnackLimit"
                                  select item.Value).Single();

            limits.DaySnackLimit = Convert.ToInt32(daySnackLimit);
            limits.MonthSnackLimit = Convert.ToInt32(monthSnackLimit);

            if (limits.DaySnackLimit == null || limits.MonthSnackLimit == null)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ModelState);

            }

            return Request.CreateResponse(limits);
        }









    }
}
