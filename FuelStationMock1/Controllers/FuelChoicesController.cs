﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using FuelStationMock1.Models.Rev2;

namespace FuelStationMock1.Controllers
{
    public class FuelChoicesController : ApiController
    {
        private FsContext db = new FsContext();

        // GET: api/FuelChoices
        public IQueryable<Choice> GetFuelChoices()
        {
            return db.Choices;
        }

        // GET: api/FuelChoices/5
        [ResponseType(typeof(Choice))]
        public async Task<IHttpActionResult> Get(int id)
        {
            Choice fuelChoice = await db.Choices.FindAsync(id);
            if (fuelChoice == null)
            {
                return NotFound();
            }

            return Ok(fuelChoice);
        }

        // PUT: api/FuelChoices/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> Put(int id, Choice fuelChoice)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != fuelChoice.ChoiceID)
            {
                return BadRequest();
            }

            db.Entry(fuelChoice).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!FuelChoiceExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/FuelChoices
        [ResponseType(typeof(Choice))]
        public async Task<IHttpActionResult> Post(Choice fuelChoice)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Choices.Add(fuelChoice);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApiPost", new { id = fuelChoice.ChoiceID }, fuelChoice);
        }

        // DELETE: api/FuelChoices/5
        [ResponseType(typeof(Choice))]
        public async Task<IHttpActionResult> Delete(int id)
        {
            Choice fuelChoice = await db.Choices.FindAsync(id);
            if (fuelChoice == null)
            {
                return NotFound();
            }

            db.Choices.Remove(fuelChoice);
            await db.SaveChangesAsync();

            return Ok(fuelChoice);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool FuelChoiceExists(int id)
        {
            return db.Choices.Count(e => e.ChoiceID == id) > 0;
        }
    }
}