﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace FuelStationMock1.Models.ViewModels
{
    public class SportCodeVM
    {
        [ReadOnly(true)]
        public string SportCodeID { get; set; }

        [ReadOnly(true)]
        public string SportDescription { get; set; }

        public DateTime? InSeasonStartDate { get; set; }
        public DateTime? InSeasonEndDate { get; set; }
        public DateTime? OffSeasonStartDate { get; set; }
        public DateTime? OffSeasonEndDate { get; set; }
    }
}