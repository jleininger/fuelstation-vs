﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FuelStationMock1.Models
{
    public class CheckoutHistory
    {
        public int DayPreCount { get; set; }
        public int DayPostCount { get; set; }
        public int DayHydrationCount { get; set; }
        public int DaySnacksCount { get; set; }
        public int MonthSnacksCount { get; set; }

    }
}