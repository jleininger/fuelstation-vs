﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FuelStationMock1.Models.Rev2
{
    public class DefaultChoice
    {
        public int DefaultChoiceID { get; set; }

        public string SportCodeID { get; set; }

        public int ChoiceID { get; set; }

        public string FuelStage { get; set; }

        public virtual Choice Choice { get; set; }
    }
}