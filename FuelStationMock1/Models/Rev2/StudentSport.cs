namespace FuelStationMock1.Models.Rev2
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("StudentSport")]
    public partial class StudentSport
    {
        [Required]
        [StringLength(20)]
        public string studentid { get; set; }

        [Required]
        [StringLength(50)]
        public string schoolsidnumber { get; set; }

        [StringLength(50)]
        public string lastname { get; set; }

        [StringLength(50)]
        public string firstname { get; set; }

        [Required]
        [StringLength(3)]
        public string SportCodeID { get; set; }

        public int StudentSportID { get; set; }

        public string FullName { get { return firstname + " " + lastname; } }

        public virtual SportCode SportCode { get; set; }

    }
}
