namespace FuelStationMock1.Models.Rev2
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Choice
    {
        //public Choice()
        //{
        //    CheckoutChoices = new HashSet<CheckoutChoice>();
        //}

        public int ChoiceID { get; set; }

        public int CategoryID { get; set; }

        [Required]
        public string Name { get; set; }

        public string Description { get; set; }

        [Required]
        public int Type { get; set; }

        [Required]
        public bool IsActive { get; set; }

        //public virtual Category Category { get; set; }

        //public virtual ICollection<CheckoutChoice> CheckoutChoices { get; set; }
    }
}
