namespace FuelStationMock1.Models.Rev2
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class SportCode
    {
        [StringLength(3)]
        [ReadOnly(true)]
        public string SportCodeID { get; set; }

        [StringLength(50)]
        public string SportDescription { get; set; }

        public DateTime? InSeasonStartDate { get; set; }
        public DateTime? InSeasonEndDate { get; set; }
        public DateTime? OffSeasonStartDate { get; set; }
        public DateTime? OffSeasonEndDate { get; set; }

        //public virtual SportCode SportCodes1 { get; set; }

        //public virtual SportCode SportCode1 { get; set; }
    }
}
