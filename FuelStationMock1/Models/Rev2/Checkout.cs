namespace FuelStationMock1.Models.Rev2
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Checkout
    {
        public int CheckoutID { get; set; }

        [Required]
        public int StudentSportID { get; set; }

        public bool isArchived { get; set; }

        public virtual StudentSport StudentSport { get; set; }

        public virtual ICollection<CheckoutChoice> CheckoutChoices { get; set; }

        [Required]
        public DateTime CreateDate { get; set; }

        public DateTime? ArchiveDate { get; set; }

        public string WorkoutStage { get; set; }

    }
}
