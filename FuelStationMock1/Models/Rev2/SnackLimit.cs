﻿namespace FuelStationMock1.Models.Rev2
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class SnackLimit
    {
        [Required]
        public int Id { get; set; }

        [Required]
        public string SportCodeId { get; set; }

        public virtual SportCode Sport { get; set; }

        [Required]
        public int DaySnackLimit { get; set; }

        [Required]
        public int MonthSnackLimit { get; set; }
    }
}
