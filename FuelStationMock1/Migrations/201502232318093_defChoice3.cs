namespace FuelStationMock1.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class defChoice3 : DbMigration
    {
        public override void Up()
        {
            CreateIndex("dbo.DefaultChoices", "ChoiceID");
            AddForeignKey("dbo.DefaultChoices", "ChoiceID", "dbo.Choices", "ChoiceID", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.DefaultChoices", "ChoiceID", "dbo.Choices");
            DropIndex("dbo.DefaultChoices", new[] { "ChoiceID" });
        }
    }
}
