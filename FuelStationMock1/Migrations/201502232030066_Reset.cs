namespace FuelStationMock1.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Reset : DbMigration
    {
        public override void Up()
        {
            //CreateTable(
            //    "dbo.Categories",
            //    c => new
            //        {
            //            CategoryID = c.Int(nullable: false, identity: true),
            //            Name = c.String(nullable: false),
            //            Description = c.String(),
            //        })
            //    .PrimaryKey(t => t.CategoryID);
            
            //CreateTable(
            //    "dbo.Choices",
            //    c => new
            //        {
            //            ChoiceID = c.Int(nullable: false, identity: true),
            //            CategoryID = c.Int(nullable: false),
            //            Name = c.String(nullable: false),
            //            Description = c.String(),
            //        })
            //    .PrimaryKey(t => t.ChoiceID)
            //    .ForeignKey("dbo.Categories", t => t.CategoryID, cascadeDelete: true)
            //    .Index(t => t.CategoryID);
            
            //CreateTable(
            //    "dbo.CheckoutChoices",
            //    c => new
            //        {
            //            CheckoutChoiceID = c.Int(nullable: false, identity: true),
            //            CheckoutID = c.Int(nullable: false),
            //            ChoiceID = c.Int(nullable: false),
            //        })
            //    .PrimaryKey(t => t.CheckoutChoiceID)
            //    .ForeignKey("dbo.Choices", t => t.ChoiceID, cascadeDelete: true)
            //    .ForeignKey("dbo.Checkouts", t => t.CheckoutID, cascadeDelete: true)
            //    .Index(t => t.CheckoutID)
            //    .Index(t => t.ChoiceID);
            
            //CreateTable(
            //    "dbo.Checkouts",
            //    c => new
            //        {
            //            CheckoutID = c.Int(nullable: false, identity: true),
            //            StudentSportID = c.Int(nullable: false),
            //            isArchived = c.Boolean(nullable: false),
            //            CreateDate = c.DateTime(nullable: false),
            //            ArchiveDate = c.DateTime(),
            //        })
            //    .PrimaryKey(t => t.CheckoutID)
            //    .ForeignKey("dbo.StudentSport", t => t.StudentSportID, cascadeDelete: true)
            //    .Index(t => t.StudentSportID);
            
            //CreateTable(
            //    "dbo.StudentSport",
            //    c => new
            //        {
            //            StudentSportID = c.Int(nullable: false, identity: true),
            //            studentid = c.String(nullable: false, maxLength: 20),
            //            schoolsidnumber = c.String(nullable: false, maxLength: 50),
            //            lastname = c.String(maxLength: 50),
            //            firstname = c.String(maxLength: 50),
            //            SportCodeID = c.String(nullable: false, maxLength: 3),
            //        })
            //    .PrimaryKey(t => t.StudentSportID)
            //    .ForeignKey("dbo.SportCodes", t => t.SportCodeID, cascadeDelete: true)
            //    .Index(t => t.SportCodeID);
            
            //CreateTable(
            //    "dbo.SportCodes",
            //    c => new
            //        {
            //            SportCodeID = c.String(nullable: false, maxLength: 3),
            //            SportDescription = c.String(maxLength: 50),
            //        })
            //    .PrimaryKey(t => t.SportCodeID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Checkouts", "StudentSportID", "dbo.StudentSport");
            DropForeignKey("dbo.StudentSport", "SportCodeID", "dbo.SportCodes");
            DropForeignKey("dbo.CheckoutChoices", "CheckoutID", "dbo.Checkouts");
            DropForeignKey("dbo.CheckoutChoices", "ChoiceID", "dbo.Choices");
            DropForeignKey("dbo.Choices", "CategoryID", "dbo.Categories");
            DropIndex("dbo.StudentSport", new[] { "SportCodeID" });
            DropIndex("dbo.Checkouts", new[] { "StudentSportID" });
            DropIndex("dbo.CheckoutChoices", new[] { "ChoiceID" });
            DropIndex("dbo.CheckoutChoices", new[] { "CheckoutID" });
            DropIndex("dbo.Choices", new[] { "CategoryID" });
            DropTable("dbo.SportCodes");
            DropTable("dbo.StudentSport");
            DropTable("dbo.Checkouts");
            DropTable("dbo.CheckoutChoices");
            DropTable("dbo.Choices");
            DropTable("dbo.Categories");
        }
    }
}
