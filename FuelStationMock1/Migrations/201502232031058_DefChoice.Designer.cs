// <auto-generated />
namespace FuelStationMock1.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.1-30610")]
    public sealed partial class DefChoice : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(DefChoice));
        
        string IMigrationMetadata.Id
        {
            get { return "201502232031058_DefChoice"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
