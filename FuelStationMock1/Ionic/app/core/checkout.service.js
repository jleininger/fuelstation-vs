(function () {
    'use strict';

    angular.module('app.core')

    .factory('CheckoutSvc', function ($resource, ApiEndpoint, IonicAlertSvc, LoadingSpinner) {
        var service = {
            archived: _archived,
            checkout: _checkout,
            currentCheckout: {},
            getCheckoutHistory: _getCheckoutHistory,
            getDailyCheckouts: _getDailyCheckouts,
            getMonthCounts: _getMonthCounts,
            fillCheckoutObject: _fillCheckoutObject,
            processCheckout: _processCheckout,
            setArchiveProperties: _setArchiveProperties,
            setUnarchiveProperties: _setUnarchiveProperties,
            unarchived: _unarchived
        };

        return service;


        function _archived() {
            return $resource(ApiEndpoint.url + 'Checkouts/Archived');
        }

        function _checkout() {
            return $resource(ApiEndpoint.url + 'Checkouts/:checkoutID', {
                checkoutID: '@checkoutID'
            }, {
                'update': {
                    method: 'PUT'
                }
            });
        }

        function _getCheckoutHistory() {
            return $resource(ApiEndpoint.url + 'Checkouts/History/:studentSportID', {
                studentSportID: '@studentSportID'
            });
        }
        
        function _getDailyCheckouts() {
            return $resource(ApiEndpoint.url + 'Checkouts/GetDailyCheckouts');
        }
        
        /**
        * Gets total orders for each snack by month
        */
        function _getMonthCounts(timePeriod){
            return $resource(ApiEndpoint.url + 'Checkouts/GetMonthCounts/Month/' + timePeriod.month + '/Year/' + timePeriod.year);
        }

        /**
        * Sets required data properties on checkout required for database model
        */
        function _fillCheckoutObject(order, studentSportID) {
            var checkout = {};

            checkout.createDate = moment().format();
            checkout.studentSportID = studentSportID;
            checkout.checkoutChoices = order;

            return checkout;
        }

        function _processCheckout(orderItems, studentSportID) {
            LoadingSpinner.show();
            
            var checkout = service.fillCheckoutObject(orderItems, studentSportID);

            var saveCheckout = service.checkout().save(checkout);
            saveCheckout.$promise.then(onOrderSuccess, IonicAlertSvc.error);

            function onOrderSuccess() {
                LoadingSpinner.hide();
                var msg = {
                    title: "Thank you for placing your order",
                    redirect: "tab.studentID"
                };
                IonicAlertSvc.alert(msg);
            }
        }

        function _setArchiveProperties(checkout) {
            var currentCheckout = checkout;

            currentCheckout.isArchived = true;
            currentCheckout.archiveDate = moment().format();
            currentCheckout.studentSport = null;

            return currentCheckout;
        }
        
        /**
        * Remove archive properties if order is unarchived to be edited or deleted
        */
        function _setUnarchiveProperties(checkout) {
            var currentCheckout = checkout;

            currentCheckout.isArchived = false;
            currentCheckout.archiveDate = null;
            currentCheckout.studentSport = null;

            return currentCheckout;
        }

        function _unarchived() {
            return $resource(ApiEndpoint.url + 'Checkouts/Unarchived');
        }


    });
})();
