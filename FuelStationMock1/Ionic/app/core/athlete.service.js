(function () {
    'use strict';

    angular.module('app.core')

    .factory('AthleteSvc', function ($resource, $cacheFactory, ApiEndpoint) {
        var service = {
            getAthlete: _getAthlete,
            athleteApi: _athleteApi
        };

        return service;

        function _athleteApi() {
            return $resource(ApiEndpoint.url + 'StudentSport/:schoolsidnumber', {
                schoolsidnumber: "@id"
            });
        }

        function _getAthlete(id) {
            return service.athleteApi().query({
                schoolsidnumber: id
            });
        }
    });
})();
